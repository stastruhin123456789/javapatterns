package structural.bridge1;

public class Skoda implements Make {
    @Override
    public void setMake() {
        System.out.println("Skoda");
    }
}
