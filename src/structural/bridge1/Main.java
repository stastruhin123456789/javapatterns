package structural.bridge1;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Sedan(new Kia());
        car1.showDetails();

        Car car2 = new Hatchback(new Skoda());
        car2.showDetails();
    }
}
