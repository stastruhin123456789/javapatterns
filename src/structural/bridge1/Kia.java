package structural.bridge1;

public class Kia implements Make {
    @Override
    public void setMake() {
        System.out.println("Kia");
    }
}
