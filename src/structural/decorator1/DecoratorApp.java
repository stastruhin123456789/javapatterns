package structural.decorator1;

public class DecoratorApp {
    public static void main(String[] args) {
//        PrinterInterface printer = new Printer("Hello");
//        PrinterInterface printer = new QuotesDecorator(new Printer("Hello"));
        PrinterInterface printer = new QuotesDecorator(new RightBracketDecorator(new LeftBracketDecorator(new Printer("Hello"))));
        printer.print();
    }
}
