package structural.decorator1;

public interface PrinterInterface {
    public void print();
}
