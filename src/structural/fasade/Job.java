package structural.fasade;

public class Job {
    public void doJob() {
        System.out.println("Job in progress...");
    }
}
