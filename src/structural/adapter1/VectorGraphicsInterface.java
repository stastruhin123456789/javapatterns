package structural.adapter1;

public interface VectorGraphicsInterface {
    public void drawLine();

    public void drawSquare();
}
