package structural.adapter1;

public class RasterGraphics {
    public void drawRasterLine() {
        System.out.println("Рисуем линию");
    }

    public void drawRasterSquare() {
        System.out.println("Рисуем квадрат");
    }
}
