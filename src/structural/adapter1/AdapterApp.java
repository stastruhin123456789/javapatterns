package structural.adapter1;

public class AdapterApp {
    public static void main(String[] args) {
        //через наследование
        VectorGraphicsInterface g1 = new VectorAdapterFromRaster();
        g1.drawLine();
        g1.drawSquare();

        //через композицию
//        VectorGraphicsInterface g2 = new VectorAdapterFromRaster2();
        VectorGraphicsInterface g2 = new VectorAdapterFromRaster2(new RasterGraphics());
        g1.drawLine();
        g1.drawSquare();

    }
}
