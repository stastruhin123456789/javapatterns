package structural.adapter1;

public class VectorAdapterFromRaster2 implements VectorGraphicsInterface {
    //RasterGraphics rasterGraphics = new RasterGraphics();
    RasterGraphics rasterGraphics = null;

    public VectorAdapterFromRaster2(RasterGraphics rasterGraphics) {
        this.rasterGraphics = rasterGraphics;
    }

    @Override
    public void drawLine() {
        rasterGraphics.drawRasterLine();
    }

    @Override
    public void drawSquare() {
        rasterGraphics.drawRasterSquare();
    }
}
