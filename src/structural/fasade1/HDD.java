package structural.fasade1;

public class HDD {
    public void copyFromDVD(DVDRom dvd) {
        if (dvd.hasData()) {
            System.out.println("Происходит копирование данных с диска");
        } else {
            System.out.println("Вставте диск с данными");
        }
    }
}
