package structural.fasade1;

public class Power {
    public void on() {
        System.out.println("Включение питания");
    }

    public void off() {
        System.out.println("Выключение питания");
    }
}
