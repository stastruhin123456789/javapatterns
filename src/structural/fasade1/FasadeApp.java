package structural.fasade1;

public class FasadeApp {
    public static void main(String[] args) {
        Computer computer = new Computer();

        computer.copy();
    }
}
