package structural.flyweight1;

public class Circle implements Shape {
    private int r = 5;

    @Override
    public void draw(int x, int y) {
        System.out.println("(" + x + ", " + y + "): рисуем круг с радиусом " + r);
    }
}
