package structural.flyweight1;

public class Square implements Shape {
    private int a = 10;

    @Override
    public void draw(int x, int y) {
        System.out.println("(" + x + ", " + y + "): рисуем квадрат со стороной " + a);
    }
}
