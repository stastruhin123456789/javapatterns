package structural.flyweight1;

public interface Shape {
    public void draw(int x, int y);
}
