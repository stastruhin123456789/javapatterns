package structural.proxy1;

public interface Image {
    public void display();
}
