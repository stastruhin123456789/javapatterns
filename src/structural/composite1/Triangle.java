package structural.composite1;

public class Triangle implements Shape {
    @Override
    public void draw() {
        System.out.println("Triangle.");
    }
}
