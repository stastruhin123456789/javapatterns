package structural.delegate1;

public interface Graphics {
    public void draw();
}
