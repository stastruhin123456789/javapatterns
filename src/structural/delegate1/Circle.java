package structural.delegate1;

public class Circle implements Graphics {
    @Override
    public void draw() {
        System.out.println("Рисуем круг");
    }
}
