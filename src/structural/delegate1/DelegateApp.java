package structural.delegate1;

public class DelegateApp {
    public static void main(String[] args) {
        Painter painter = new Painter();
        painter.setGraphics(new Circle());
        painter.draw();

        painter.setGraphics(new Triangle());
        painter.draw();
    }
}
