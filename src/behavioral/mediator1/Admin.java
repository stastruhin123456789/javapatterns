package behavioral.mediator1;

public class Admin extends User {

    public Admin(Chat chat, String name) {
        super(chat, name);
    }

    @Override
    public void getMessage(String message) {
        System.out.println("Администратор получает сообщение '" + message + "'");
    }
}
