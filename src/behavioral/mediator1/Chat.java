package behavioral.mediator1;

public interface Chat {
    public void sendMessage(String message, User user);
}
