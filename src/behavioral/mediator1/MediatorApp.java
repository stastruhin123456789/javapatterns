package behavioral.mediator1;


public class MediatorApp {
    public static void main(String[] args) {
        TextChat chat = new TextChat();

        User admin = new Admin(chat, "Admin");
        User u1 = new SimpleUser(chat, "User 1");
        User u2 = new SimpleUser(chat, "User 2");
        User u3 = new SimpleUser(chat, "User 3");
        u2.setEnable(false);

        chat.setAdmin(admin);
        chat.addUser(u1);
        chat.addUser(u2);
        chat.addUser(u3);

//        u1.sendMessage("Привет");
        admin.sendMessage("Привет");
    }
}
