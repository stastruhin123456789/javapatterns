package behavioral.state1;

public class Radio {
    private Station station;

    public void setStation(Station station) {
        this.station = station;
    }

    public void nextStation() {
        if (station instanceof HitFM) {
            setStation(new KissFM());
        } else if (station instanceof KissFM) {
            setStation(new RadioRecord());
        } else if (station instanceof RadioRecord) {
            setStation(new HitFM());
        }
    }

    public void play() {
        station.play();
    }
}
