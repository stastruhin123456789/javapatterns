package behavioral.state1;

public interface Station {
    public void play();
}
