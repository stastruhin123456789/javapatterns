package behavioral.state1;

public class StateApp {
    public static void main(String[] args) {
        //example1
        Station hitFM = new HitFM();
        Radio radio = new Radio();
        radio.setStation(hitFM);

        for (int i = 0; i < 10; i++) {
            radio.play();
            radio.nextStation();
        }

        //example2
        Human human = new Human();
        human.setState(new Work());
        for (int i = 0; i < 10; i++) {
            human.doSomething();
        }
    }
}
