package behavioral.state1;

public interface Activity {
    public void doSomething(Human context);
}
