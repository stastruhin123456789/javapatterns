package behavioral.state1;

public class RadioRecord implements Station {
    @Override
    public void play() {
        System.out.println("Radio Record...");
    }
}
