package behavioral.strategy1;

public interface Sorting {
    public void sort(int[] arr);
}
