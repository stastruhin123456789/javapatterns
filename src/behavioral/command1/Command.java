package behavioral.command1;

public interface Command {
    public void execute();
}
