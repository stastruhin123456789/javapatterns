package behavioral.observer1;

import java.util.ArrayList;
import java.util.List;

public class MeteoStation implements Observed {
    private int temperature;
    private int pressure;

    private List<Observer> observers = new ArrayList<>();

    public void setMeasurements(int temperature, int pressure) {
        this.temperature = temperature;
        this.pressure = pressure;
        notifyObservers();
    }

    @Override
    public void addObserver(Observer o) {
        observers.add(o);
    }

    @Override
    public void removeObserver(Observer o) {
        observers.remove(o);
    }

    @Override
    public void notifyObservers() {
        for (Observer o : observers) {
            o.handlerEvent(temperature, pressure);
        }
    }
}
