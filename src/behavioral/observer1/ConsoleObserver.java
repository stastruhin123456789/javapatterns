package behavioral.observer1;

public class ConsoleObserver implements Observer {
    @Override
    public void handlerEvent(int temp, int presser) {
        System.out.println("Погода изменилась. Температура = " + temp + ", Давление = " + presser + ".");
    }
}
