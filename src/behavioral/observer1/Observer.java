package behavioral.observer1;

public interface Observer {
    public void handlerEvent(int temp, int presser);
}
