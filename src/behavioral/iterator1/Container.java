package behavioral.iterator1;

public interface Container {
    public Iterator getIterator();
}
