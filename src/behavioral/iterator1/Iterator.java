package behavioral.iterator1;

public interface Iterator {
    public boolean hasNext();

    public Object next();
}
