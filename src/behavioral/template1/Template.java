package behavioral.template1;

public abstract class Template {
    public void method() {
        System.out.print("1");
        differ();
        System.out.print("3");
        differ2();
    }

    public abstract void differ();

    public abstract void differ2();
}
