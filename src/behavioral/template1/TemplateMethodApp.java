package behavioral.template1;

public class TemplateMethodApp {
    public static void main(String[] args) {
        A a = new A();
        a.method();

        System.out.println();

        B b = new B();
        b.method();
    }
}
