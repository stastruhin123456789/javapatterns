package behavioral.visitor1;

public interface Visitor {
    public void visit(EngineElement engine);

    public void visit(BodyElement body);

    public void visit(CarElement car);

    public void visit(WheelElement wheel);
}
