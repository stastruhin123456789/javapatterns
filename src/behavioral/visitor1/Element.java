package behavioral.visitor1;

public interface Element {
    public void accept(Visitor visitor);
}
