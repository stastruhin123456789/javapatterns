package behavioral.interpreter1;

public interface Expression {
    int interpret();
}
