package behavioral.chain1;

public class BankomatApp {
    public static void main(String[] args) {
        NoteModule note1000 = new NoteModule1000();
        NoteModule note500 = new NoteModule500();
        NoteModule note200 = new NoteModule200();
        NoteModule note100 = new NoteModule100();
        NoteModule note50 = new NoteModule50();
        note1000.setNext(note500);
        note500.setNext(note200);
        note200.setNext(note100);
        note100.setNext(note50);

        note1000.takeMoney(new Money(1850));
    }
}
