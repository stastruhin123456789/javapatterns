package behavioral.chain1;

public abstract class Logger {
    private int priority;

    public Logger(int priority) {
        this.priority = priority;
    }

    protected Logger next;

    public void setNext(Logger next) {
        this.next = next;
    }

    public void writeMessage(String message, int level) {
        if (level <= priority) {
            write(message);
        }
        if (next != null) {
            next.writeMessage(message, level);
        }
    }

    protected abstract void write(String message);

}
