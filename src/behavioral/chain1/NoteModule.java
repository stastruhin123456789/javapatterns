package behavioral.chain1;

public abstract class NoteModule {
    protected NoteModule next;

    public void setNext(NoteModule next) {
        this.next = next;
    }

    public abstract void takeMoney(Money money);
}
