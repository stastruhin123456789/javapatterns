package behavioral.chain1;

public class NoteModule50 extends NoteModule {
    @Override
    public void takeMoney(Money money) {
        int note = Note.U50;
        int countNote = money.getAmt() / Note.U50;
        int remind = money.getAmt() % Note.U50;
        if (countNote > 0) {
            System.out.println("Выдано " + countNote + " купюра достоинством " + Note.U50);
        }
        if (remind > 0 && next != null) {
            next.takeMoney(new Money(remind));
        }
    }
}
