package behavioral.chain1;

public class NoteModule200 extends NoteModule {
    @Override
    public void takeMoney(Money money) {
        int countNote = money.getAmt() / Note.U200;
        int remind = money.getAmt() % Note.U200;
        if (countNote > 0) {
            System.out.println("Выдано " + countNote + " купюра достоинством " + Note.U200);
        }
        if (remind > 0 && next != null) {
            next.takeMoney(new Money(remind));
        }
    }
}
