package behavioral.chain1;

public class NoteModule500 extends NoteModule {
    @Override
    public void takeMoney(Money money) {
        int countNote = money.getAmt() / Note.U500;
        int remind = money.getAmt() % Note.U500;
        if (countNote > 0) {
            System.out.println("Выдано " + countNote + " купюра достоинством " + Note.U500);
        }
        if (remind > 0 && next != null) {
            next.takeMoney(new Money(remind));
        }
    }
}
