package behavioral.chain1;

public class NoteModule100 extends NoteModule {
    @Override
    public void takeMoney(Money money) {
        int countNote = money.getAmt() / Note.U100;
        int remind = money.getAmt() % Note.U100;
        if (countNote > 0) {
            System.out.println("Выдано " + countNote + " купюра достоинством " + Note.U100);
        }
        if (remind > 0 && next != null) {
            next.takeMoney(new Money(remind));
        }
    }
}
