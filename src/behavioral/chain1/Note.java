package behavioral.chain1;

public class Note {
    public static final int U50 = 50;
    public static final int U100 = 100;
    public static final int U200 = 200;
    public static final int U500 = 500;
    public static final int U1000 = 1000;
}
