package behavioral.chain1;

public class Money {
    private int amt;

    public Money(int amt) {
        setAmt(amt);
    }

    public int getAmt() {
        return amt;
    }

    public void setAmt(int amt) {
        if (amt > 0 && amt <= 25_000 && amt % (Note.U50) == 0) {
            this.amt = amt;
        } else {
            throw new RuntimeException("Сумма денег должна быть не более " + 25_000 + " и кратна " + Note.U50);
        }
    }
}
