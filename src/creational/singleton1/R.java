package creational.singleton1;

public class R implements Runnable {
    @Override
    public void run() {
        Singleton.getInstance();
    }
}
