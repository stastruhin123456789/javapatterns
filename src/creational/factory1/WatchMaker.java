package creational.factory1;

public interface WatchMaker {
    public Watch createWatch();
}
