package creational.factory1;

public interface Watch {
    public void showTime();
}
