package creational.prototype1;

public interface Copyable {
    public Object copy();
}
