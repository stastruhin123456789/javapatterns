package creational.abstractfactory;

public interface ProjectManager {
    public void manageProject();
}
