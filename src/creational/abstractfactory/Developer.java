package creational.abstractfactory;

public interface Developer {
    public void writeCode();
}
