package creational.abstractfactory;

public interface Tester {
    public void testCode();
}
