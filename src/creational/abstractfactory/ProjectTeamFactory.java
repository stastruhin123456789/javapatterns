package creational.abstractfactory;

public interface ProjectTeamFactory {
    public Developer getDeveloper();

    public Tester getTester();

    public ProjectManager getProjectManager();
}
