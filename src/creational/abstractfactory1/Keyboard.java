package creational.abstractfactory1;

public interface Keyboard {
    public void print();

    public void println();
}
