package creational.abstractfactory1;

public interface Mouse {
    public void click();

    public void dblclick();

    public void scroll(int direction);
}
