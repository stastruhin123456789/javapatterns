package creational.abstractfactory1;

class EnKeyboard implements Keyboard {
    public void print() {
        System.out.print("Print");
    }

    public void println() {
        System.out.println("Print Line");
    }
}