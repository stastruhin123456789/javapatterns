package creational.abstractfactory1;

public interface Touchpad {
    public void track(int deltaX, int deltaY);
}
