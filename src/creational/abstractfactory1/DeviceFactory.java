package creational.abstractfactory1;

public interface DeviceFactory {
    public Mouse getMouse();

    public Keyboard getKeyboard();

    public Touchpad getTouchpad();
}
