package creational.builder1;

public class BuilderApp {
    public static void main(String[] args) {
//        Car car = new CarBuilder()
//                .buildMake("Mercsdes")
//                .buildTransmission(Transmission.AUTO)
//                .buildMaxSpeed(280)
//                .build();
//        System.out.println(car);
        Director director = new Director();
        director.setBuilder(new SubaruBuilder());
        Car car = director.BuilderCar();
        System.out.println(car);
    }
}
