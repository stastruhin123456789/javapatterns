package creational.builder1;

public class FordMondeoBuilder extends CarBuilder {
    @Override
    public void builderMake() {
        car.setMake("Ford Mondeo");
    }

    @Override
    public void buildTransmission() {
        car.setTransmission(Transmission.AUTO);
    }

    @Override
    public void buildMaxSpeed() {
        car.setMaxSpeed(260);
    }
}
