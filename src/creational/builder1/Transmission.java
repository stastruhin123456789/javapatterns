package creational.builder1;

public enum Transmission {
    MANUAL, AUTO;
}
