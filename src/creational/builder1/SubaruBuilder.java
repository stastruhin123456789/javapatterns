package creational.builder1;

public class SubaruBuilder extends CarBuilder {
    @Override
    public void builderMake() {
        car.setMake("Subaru");
    }

    @Override
    public void buildTransmission() {
        car.setTransmission(Transmission.MANUAL);
    }

    @Override
    public void buildMaxSpeed() {
        car.setMaxSpeed(320);
    }
}
