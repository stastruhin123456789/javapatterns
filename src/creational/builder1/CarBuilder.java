package creational.builder1;

abstract class CarBuilder {
    protected Car car;

    public void createCar() {
        car = new Car();
    }

    public abstract void builderMake();

    public abstract void buildTransmission();

    public abstract void buildMaxSpeed();

    public Car getCar() {
        return car;
    }

//    private String m = "Default";
//    private Transmission t = Transmission.MANUAL;
//    private int s = 120;
//
//    public CarBuilder buildMake(String m) {
//        this.m = m;
//        return this;
//    }
//
//    public CarBuilder buildTransmission(Transmission t) {
//        this.t = t;
//        return this;
//    }
//
//    public CarBuilder buildMaxSpeed(int s) {
//        this.s = s;
//        return this;
//    }
//
//    public Car build() {
//        Car car = new Car();
//        car.setMake(m);
//        car.setTransmission(t);
//        car.setMaxSpeed(s);
//        return car;
//    }
}
