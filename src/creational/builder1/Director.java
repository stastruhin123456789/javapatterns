package creational.builder1;

public class Director {
    private CarBuilder builder;

    public void setBuilder(CarBuilder b) {
        builder = b;
    }

    public Car BuilderCar() {
        builder.createCar();
        builder.builderMake();
        builder.buildTransmission();
        builder.buildMaxSpeed();
        Car car = builder.getCar();
        return car;
    }
}
